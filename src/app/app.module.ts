import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { Camera } from '@ionic-native/camera';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ImagePicker } from '@ionic-native/image-picker';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Autostart  } from '@ionic-native/autostart';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AdMobFree } from '@ionic-native/admob-free';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PetDataServiceProvider } from '../providers/pet-data-service/pet-data-service';
import { InfoDataProvider } from '../providers/info-data/info-data';
import { DateProvider } from '../providers/date/date';
import { MyCameraProvider } from '../providers/my-camera/my-camera';
import { ReminderServiceProvider } from '../providers/reminder-service/reminder-service';
import { BackgroundServiceProvider } from '../providers/background-service/background-service';
import { ScheduleNotificationsProvider } from '../providers/schedule-notifications/schedule-notifications';
import { OnAppStartProvider } from '../providers/on-app-start/on-app-start';
import { AlertPopupProvider } from '../providers/alert-popup/alert-popup';
import { AdmobServiceProvider } from '../providers/admob-service/admob-service';
import { LoadingSpinnerProvider } from '../providers/loading-spinner/loading-spinner';



@NgModule({
  declarations: [
    MyApp,
    
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},    
    Camera,
    ImagePicker,
    Diagnostic,
    BackgroundMode,
    Autostart,
    DatePipe,
    LocalNotifications,
    AdMobFree,
    PetDataServiceProvider,
    InfoDataProvider,
    DateProvider,
    MyCameraProvider,
    ReminderServiceProvider,
    BackgroundServiceProvider,
    ScheduleNotificationsProvider,
    OnAppStartProvider,
    AlertPopupProvider,
    AdmobServiceProvider,
    LoadingSpinnerProvider,
   
    
  ]
})
export class AppModule {}
