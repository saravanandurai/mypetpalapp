import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { BackgroundServiceProvider } from '../providers/background-service/background-service';

import { OnAppStartProvider } from '../providers/on-app-start/on-app-start';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'PetNewPage';

  pages: Array<{title: string, component: any, params: any}>;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              //public backgroundService: BackgroundServiceProvider,              
              private onAppStart: OnAppStartProvider,
              ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage', params: { "title":"Home Page"} },
      //{ title: 'List', component: 'ListPage' },
      { title: 'Add Pet', component: 'AddEditPetPage', params: { "title":"Add Pet"} },
      { title: 'Add Info', component: 'AddEditInfoPage', params: { "title": "Add Info"} },
      { title: 'Info List', component: 'MoreInfoPage', params: { "title":"More Info"} },
      { title: 'Add Reminder', component: 'AddEditReminderPage',  params: { "title": "Add Reminder"}},
      { title: 'Reminders List', component: 'RemindersListPage', params: { "title":"Reminder List"}  },
      { title: 'About' , component: 'AboutPage', params: { "title":"About Page"}},
      { title: 'Help', component: 'HelpPage', params: { "title":"Help Page"}},
      { title: 'Pet New', component: 'PetNewPage', params: { "title": "Add Pet"}},
      
      

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();     
     
     //commented to use localhost server for dev
     this.onAppStart.appInitialRun();
     //this.onAppStart.callScheduleNotifications();

     //commented for release 1 since it is not required for the app. 
     // this.backgroundService.start();
     
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, page.params);
  }
}
