export interface Info{
    "id": number;
    "date": string;
    "type":string;
    "details":string;
    "pid":number;
    "pname":string;
    "img":string[];

}