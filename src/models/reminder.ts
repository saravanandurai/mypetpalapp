export interface Reminder{
    id: number;
    date: string;
    pid: number;
    pname: string;
    type: string;
    details: string;
    notifications: {
        onTheDay: boolean;
        oneDayBefore: boolean;
        twoDaysBefore: boolean;
        threeDaysBefore: boolean;
        oneWeekBefore: boolean;
        twoWeeksBefore: boolean;
        threeWeeksBefore: boolean;
        oneMonthBefore: boolean;
    };
    


}