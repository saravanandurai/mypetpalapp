export interface Pet {
    
    id: number;
    name: string;
    dob: string;
    type: string;
    breedName: string;
    gender: string;
    img: string
    
    
}