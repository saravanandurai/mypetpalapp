import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { GalleryViewComponent } from './gallery-view';
import { GalleryModalPageModule } from '../../pages/gallery-modal/gallery-modal.module'

@NgModule({
  declarations: [
    GalleryViewComponent,
  ],
  imports: [
    GalleryModalPageModule,
    IonicModule,
  ],
  exports: [
    GalleryViewComponent
  ]
})
export class GalleryViewComponentModule {}
