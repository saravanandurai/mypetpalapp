import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { ModalController } from 'ionic-angular';
//import { GalleryModalPage } from "../../pages/gallery-modal/gallery-modal";


/**
 * Generated class for the GalleryViewComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */

@Component({
  selector: 'gallery-view',
  templateUrl: 'gallery-view.html'
})
export class GalleryViewComponent {
  @Input('options') galViewOptions;
  @Output() dataRefresh = new EventEmitter(); 
  
  
  
  twoPicRow: boolean = false;
  onePicRow: boolean = false;
  fullWidthFlag:boolean = false;
  lastImg: string;
  dispImg: any = [{"first":"","second":""}];
  picsLen : number;
  tempImg: any;
  constructor(private modalCtrl: ModalController) {
    console.log('Hello GalleryViewComponent Component');
    console.log("GalViewoptions: " + JSON.stringify(this.galViewOptions));
   // console.log("Edit Flag: " + this.galViewOptions.editFlag);
   // console.log("TempImg: " + JSON.stringify(this.galViewOptions.imgArr));
    
  }

  ionViewWillEnter(){
    console.log("IonViewWillEnter GalleryViewComponent");
  }

  ngOnChanges(){
    this.tempImg = this.galViewOptions.imgArr;
    this.picsLen = this.tempImg.length;
    this.dispImg = [{"first":"","second":""}];
    this.lastImg = "";
    this.onePicRow = false;
    this.twoPicRow = false;
    
    
    console.log("GalViewoptions1: " + JSON.stringify(this.galViewOptions));
    if((this.picsLen%2)==1){
          
          if(this.picsLen > 1){
            this.twoPicRow = true;
            for (let i =0, j=0; i < (this.tempImg.length-1)/2 && j<this.tempImg.length-1; i++,j=j+2){                       
            
            this.dispImg[i] = {"first":this.tempImg[j],"second":this.tempImg[j+1]};
            
          } 

          }
          this.onePicRow = true;          
                       
          this.lastImg = this.tempImg[this.tempImg.length-1];          
          this.imgHtWdDecider(this.lastImg).then((val)=>{
            this.fullWidthFlag = val;            
          });  
          console.log("Image Array to be Displayed")
           
        }else{
          
          this.twoPicRow = true;
          this.onePicRow = false;

          for (let i =0, j=0; i < this.tempImg.length/2 && j<this.tempImg.length; i++,j=j+2){                       
            
            this.dispImg[i] = {"first":this.tempImg[j],"second":this.tempImg[j+1]};
          }     
          
        }

        

  }

  imgHtWdDecider(imgSrc: string): Promise<boolean>{
    return new Promise(resolve=>{
      let imgNew  = new Image();
      imgNew.name = imgSrc;
      imgNew.src = imgSrc;
      imgNew.onload = function(){         
        let ht: number = imgNew.height;
        let wd: number = imgNew.width;
        console.log("Height and Width: " + ht + " & " + wd);
        let flg: boolean;
        if(wd > ht){
          flg = true; 
        }else{
          flg = false;
        }
        resolve(flg);
      }
    })
  };

  launchGalleryModal(currImg: string){
    let galModal = this.modalCtrl.create('GalleryModalPage',{"currImg":currImg, "imgArr":this.tempImg, "edit":this.galViewOptions.editFlag});
    galModal.onDidDismiss(data=>{
      
      
      let galOption = {"imgArr":data.imgArr, "editFlag":data.editFlag};      
      console.log("GalViewOptionsonDismiss: " + JSON.stringify(galOption));     
      this.dataRefresh.emit(galOption);      
      
    })
    galModal.present();
  }





}
