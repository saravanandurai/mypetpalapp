import { Injectable, Input } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { AlertController, ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Pet } from '../../models/pet';
 

/*
  Generated class for the PetDataServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PetDataServiceProvider {
  @Input()
  pet: Pet = {"id":0,"name":"","dob":new Date().toISOString(),"type":"","breedName":"", "gender":"", "img":"" };
   

  //constructor(public http: Http, private storage: Storage) {
  constructor(private storage: Storage, 
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,
            ) {
    console.log('Hello PetDataServiceProvider Provider');
  }

  petAlert(msg: string){
    let alert = this.alertCtrl.create({
      title:"Pet Saved",
      subTitle:msg,
      buttons:['Dismiss']
    });
    alert.present();
  }

  showToast(msg: string){
    let toastShow = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "middle"
    });
    toastShow.present();

  }

  getPetList(): Promise<Pet[]> {  

    return Promise.resolve(this.storage.get("pets"));
        
  }

  getPet(id: number,name: string): Promise<Pet>{
    return this.storage.get("pets").then((val)=>{
      let iPets: Pet[] = val;
      for (let i=0; i < iPets.length; i++){
        if(iPets[i].id == id){
          return Promise.resolve(iPets[i]);
        }       
    }
    })
  }

  savePet(pet: Pet){
    return(
      this.storage.get("pets").then((val)=>{
      let status: boolean = false;
      console.log("Incoming Pet: "+ JSON.stringify(pet));
      if(val == null){
        pet.id = 1;
        let pets:Pet[] = [pet];
        this.storage.set("pets",pets);
        this.showToast("First Pet '" + pet.name + "' Saved Successfully.")
        //this.petAlert("First Pet Saved Successfully: " + JSON.stringify(pet));
        //this.setSharePet(pet);
        return pet;
      }
      
      else{
        for(let i = 0; i < val.length; i++){
          if(val[i].id==pet.id){
            val[i] = pet;
            this.storage.set("pets",val);
            status = true;
            this.showToast("Pet '" + pet.name + "' updated Successfully")
            //this.petAlert("Pet updated Successfully: " + JSON.stringify(pet));
            //this.setSharePet(pet);
            return pet;
          }

        }

        if(!status){
          let lpid: number = val[val.length -1].id;
          pet.id = lpid+1;
          val[val.length]= pet;
          this.storage.set("pets",val);
          this.showToast("New Pet '" + pet.name + "' Added Successfully")
          //this.petAlert("New Pet Added Successfully: " + JSON.stringify(pet));
         
         //this.setSharePet(pet);
         return pet;
        }        
      }
      
    })    

  )}

  deleteAllPets(){
    this.storage.remove("pets");
    this.storage.get("pets").then((val)=>{
      console.log("After Removal: " + JSON.stringify(val));
    })
  }

  deletePet(pet: Pet): Promise<boolean>{
    return(
    this.storage.get("pets").then((val=>{
      let fPets: Pet[] = val;
      let rPets: Pet[];
      rPets = fPets.filter(function(fpet){
        return fpet.id != pet.id
      });
      this.storage.set("pets",rPets);
      return Promise.resolve(true);

      
    }))
    )
  }

  getLPetid(){
    return(   
    this.storage.get("pets").then((val)=>{
      let len: number = val.length;
      console.log("LPId: " +val[len-1].id);
      return val[len-1].id;
    })
    )    
    
  }

//Not required at this point in time.
  setSharePet(pet: Pet){
    this.pet = pet;
  }

  getSharePet(){
    return this.pet;
  }
}
