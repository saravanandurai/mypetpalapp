import { Injectable, Input, NgZone } from '@angular/core';
import { ActionSheetController } from 'ionic-angular';

import 'rxjs/add/operator/map';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ImagePicker } from '@ionic-native/image-picker';

/*
  Generated class for the MyCameraProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MyCameraProvider {
  @Input()
 
  options: CameraOptions = {
                                      quality: 50,
                                      destinationType: this.camera.DestinationType.FILE_URI,
                                      encodingType: this.camera.EncodingType.JPEG,
                                      mediaType: this.camera.MediaType.PICTURE,
                                      correctOrientation: true,
                                      sourceType: null,
                                      targetHeight:800,
                                      targetWidth: 600 
                                  };

  constructor(private camera: Camera, 
              private diagnostic: Diagnostic,
              private actionSheetCtrl: ActionSheetController,
              private ngZone: NgZone,
              private imagePicker: ImagePicker
              ) {
    console.log('Hello MyCameraProvider Provider');
  }



  CamOrGalSelector():  Promise<any>{
    return new Promise((resolve)=>{
    
    let ret: any;
    let actSheet = this.actionSheetCtrl.create({
      title:"Add Photo Options",
      buttons:[
        {
          text: "Take New Picture",
          handler:()=>{
            this.options.sourceType = 1;
            this.cameraOrGalCall().then((val)=>{
              console.log("New Picture: " + JSON.stringify(val));
              ret=val;
              resolve(ret);
            })
          }
        },
        {
          text: "Select Picture from Gallery",
          handler:()=>{
            this.options.sourceType = 0;
            this.cameraOrGalCall().then((val)=>{
              console.log("Select Picture: " + JSON.stringify(val));
              ret=val;
              resolve(ret);
            });
          }
        }
      ]
    });

    actSheet.present();
    })
  }

  cameraOrGalCall():Promise<any>{
    return new Promise((resolve)=>{        
       this.camera.getPicture(this.options).then((imageData)=>{

        let base64Image: string;
        this.ngZone.run(()=>{
          base64Image = imageData;
        })
        console.log("Base64: " + base64Image);
       
        let ret = {"base64Image":base64Image, "camStatus":true, "error":false, "errorMsg":"None"};
        resolve(ret);     
      
      }, (err)=>{
        console.log("Camera Error.");
    
        let ret = {"base64Image":"", "camStatus":false, "error":true, "errorMsg":JSON.stringify(err)};
        resolve(ret);         
    
      })
    })
  };
  


  mulCamorGalSelector(imgCnt: number): Promise<any>{
    return new Promise((resolve)=>{
      let ret: any;
      let retActSheet = this.actionSheetCtrl.create({
        title:"Photo Options",
        buttons:[
          {
            text:"Take New Picture",
            handler:()=>{
              this.options.sourceType = 1;
              this.cameraOrGalCall().then((val)=>{
                console.log("New Picture: " + JSON.stringify(val));
                if(val.error == false){
                  let obj = [val.base64Image];
                  ret = {"base64Image":obj, "camStatus":true, "error":false, "errorMsg":"None"};
                }else
                {
                  ret = val;
                }
                                
                resolve(ret);
              })

            }

          },
          {
          text:"Select Picture from Gallery",
          handler: ()=>{            
            this.mulImagePick(imgCnt).then((val)=>{
              console.log("Select Picture: " + JSON.stringify(val));
              ret = val;
              resolve(ret);
            })
          }
          }
        ]
      })
      retActSheet.present();
    })

  };

  mulImagePick(imgCnt: number):Promise<any>{
    return new Promise((resolve)=>{
      let maxImgNo: number = 6 - imgCnt;
      let option = {
        maximumImagesCount: maxImgNo,
        quality: 50         
      }
      this.imagePicker.getPictures(option).then((val)=>{
        console.log("Returned Val: " + JSON.stringify(val));
        let ret = {"base64Image":val, "camStatus":true, "error":false, "errorMsg":"None"};
        resolve(ret);

      }
      ,(err)=>{
        let ret = {"base64Image":"", "camStatus":false, "error":true, "errorMsg":JSON.stringify(err)};
        resolve(ret);
      })
    })
  }
  




}
