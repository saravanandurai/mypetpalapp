import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ReminderServiceProvider } from '../reminder-service/reminder-service';
import { DatePipe } from '@angular/common';
import { LocalNotifications } from '@ionic-native/local-notifications';
import * as moment from 'moment';

/*
  Generated class for the ScheduleReminderProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ScheduleNotificationsProvider {

  notes: Array<{id: number, text: string, at: string}> = [{"id":0,"text":"","at":""}];
  nots: Array<{id: number, text: string, at: string}> = []; 
   
  i: number = 0;
  id: number = 0;
  notTime: number = 12;
  constructor(private reminderService: ReminderServiceProvider,
              private datePipe: DatePipe,
              private localNotifications: LocalNotifications
              ) {
    console.log('Hello ScheduleReminderProvider Provider');
  }

    buildReminders():Promise<any>{
        return new Promise(resolve=>{
            this.reminderService.getReminderList().then(rList=>{
                    let cDt = moment(new Date());
                    let validList = rList.filter(function(rLst){
                        console.log("diffsdate: " + moment(rLst.date).diff(cDt, "days") + " && " + rLst.date + " & " + 
                        cDt.format());
                        return moment(rLst.date).diff(cDt, "days") >= 0;
                    })
                    console.log("validList: " + JSON.stringify(validList));
                    let idCtr: number = 1;
                    this.nots = [];
                for(let r of validList){
                    console.log("Nots: " + JSON.stringify(this.nots));
                    let rDt = moment(r.date);
                    rDt.set({"hour":this.notTime, "minute":0, "second":0, "millisecond":0 });
                    let rDtStr = rDt.format("DD MMM YYYY");                    
                                                           
                    let dif = rDt.diff(cDt, "days", true);
                    let difM = rDt.diff(cDt, "months", true);
                    let cRDt: Array<any> = [];
                    for(let x = 0; x < 8; x++){
                        cRDt.push(moment(rDt));
                    }
                    console.log("cRDt: " + JSON.stringify(cRDt));
                    console.log("difM: " + difM);                   
                    //console.log("rDtStr: " + rDtStr + " & rDt: " + rDt.format());
                    let textStr = "Notification for " + r.pname + "'s " + r.type + " reminder event on " + rDtStr + 
                        ". The details are: " + r.details;
                    
                    if(r.notifications.onTheDay == true && dif >= 0){                        
                        this.nots.push({"id":idCtr, "text": textStr, "at":cRDt[0].format()});
                        idCtr = idCtr + 1;
                        console.log("On");
                    }
                    if(r.notifications.oneDayBefore == true && dif >= 1){
                        this.nots.push({"id":idCtr, "text": textStr + 1, "at":cRDt[1].subtract(1,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("1");
                    }
                    if(r.notifications.twoDaysBefore == true && dif >= 2){
                        this.nots.push({"id":idCtr, "text": textStr + 2, "at":cRDt[2].subtract(2,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("2");
                    }
                    if(r.notifications.threeDaysBefore == true && dif >= 3){
                        this.nots.push({"id":idCtr, "text": textStr + 3, "at":cRDt[3].subtract(3,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("3");
                    }
                    if(r.notifications.oneWeekBefore == true && dif >= 7){
                        this.nots.push({"id":idCtr, "text": textStr + 7, "at":cRDt[4].subtract(7,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("7");
                    }
                    if(r.notifications.twoWeeksBefore == true && dif >= 14){
                        this.nots.push({"id":idCtr, "text": textStr + 14, "at":cRDt[5].subtract(14,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("14");
                    }
                    if(r.notifications.threeWeeksBefore == true && dif >= 21){
                        this.nots.push({"id":idCtr, "text": textStr + 21, "at":cRDt[6].subtract(21,"days").format()});
                        idCtr = idCtr + 1;
                        console.log("21");
                    }
                    if(r.notifications.oneMonthBefore == true && difM >= 1){
                        this.nots.push({"id":idCtr, "text": textStr + "Month", "at":cRDt[7].subtract(1,"months").format()});
                        idCtr = idCtr + 1;
                        console.log("Month");
                    }
                    //console.log('dif: ' + dif);                       
                }

                
                console.log("nots: " + JSON.stringify(this.nots));
                resolve(this.nots);
            })
        })
    }

    

    setNotifications(){
    
    this.localNotifications.getAll().then(val=>{
        console.log("Scheduled Old: len : " + val.length);
        console.log("Scheduled Notifications Old: " + JSON.stringify(val));
    });
    this.localNotifications.cancelAll().then(canNot=>{
       // if(val = true){

        
        this.buildReminders().then(builtNots=>{
            if(builtNots.length > 0 && builtNots[0].id != 0){
                let val1 = builtNots;
                console.log("Built Reminders: " + JSON.stringify(val1));
                for(let va of builtNots){                        
                    this.localNotifications.schedule({
                        id: va.id,
                        text: va.text,
                        at: moment(va.at).toDate()
                        })      
                }
                this.localNotifications.isScheduled(builtNots[builtNots.length - 1].id).then(schPro=>{
                    if(schPro == true){
                        this.localNotifications.getAll().then(valret=>{
                            console.log("Scheduled New: len : " + valret.length);
                            console.log("Scheduled Notifications New: " + JSON.stringify(valret));
                        });
                
                    }
                })       
            }
        }) 
                  
    })
    };

    getAllNotifications():Promise<any>{
        return new Promise(resolve=>{
            this.localNotifications.getAll().then(val=>{
                console.log("All Nots: " + JSON.stringify(val));
                resolve(val); 
            });
        })    
    }
  
  

}
