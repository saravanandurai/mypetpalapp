import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Autostart } from '@ionic-native/autostart';

import { ReminderServiceProvider } from '../reminder-service/reminder-service';


/*
  Generated class for the BackgroundServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BackgroundServiceProvider {

  constructor(public backgroundMode: BackgroundMode,
              private autostart: Autostart,
              private reminderService: ReminderServiceProvider) {
    console.log('Hello BackgroundServiceProvider Provider');
  }

  start(){
    this.reminderService.getReminderList().then((val)=>{
      if(val != undefined && val != null){
        this.autostart.enable();
        this.backgroundMode.enable();
        this.backgroundMode.overrideBackButton();
        console.log("Background Mode and Autostart enabled Successfully.");
      }
    })
    
  }

  stop(){
    this.backgroundMode.disable();
    this.autostart.disable();    
  }

}
