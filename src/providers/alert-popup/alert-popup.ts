import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

import { PetDataServiceProvider } from '../pet-data-service/pet-data-service';

/*
  Generated class for the AlertPopupProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AlertPopupProvider {

  constructor(private petDataService: PetDataServiceProvider,
              private alertCtrl: AlertController,
              ) {
    console.log('Hello AlertPopupProvider Provider');
  }

  petSelectAlert():Promise<string>{
    return new Promise(resolve=>{
    let petSelectAlrt = this.alertCtrl.create({"enableBackdropDismiss":false});
    petSelectAlrt.setTitle("Select Pet");
    petSelectAlrt.addButton({
      text: "Ok",
      handler: sPet =>{
        console.log("Selected Pet" + sPet);
        let sPetObj = JSON.parse(sPet);
        resolve(sPet);
        
      }
    });
    
    this.petDataService.getPetList().then(pList=>{
      for(let p of pList){
        if(p == pList[0]){
          petSelectAlrt.addInput({
          type: 'radio',
          label: p.name,
          value: JSON.stringify({"pid":p.id, "pname": p.name}),
          checked: true
        })
        }else{
          petSelectAlrt.addInput({
          type: 'radio',
          label: p.name,
          value: JSON.stringify({"pid":p.id, "pname": p.name}),          
        })
        }
        
      }
      
      petSelectAlrt.present();
    })
  })    
  }

}
