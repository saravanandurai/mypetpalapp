import { Injectable, Input } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';
import { AlertController, ToastController } from 'ionic-angular';

import { Info } from '../../models/info';
import * as moment from 'moment';

/*
  Generated class for the InfoDataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class InfoDataProvider {
  @Input()
  info: Info = {"id":0, "date":new Date().toISOString(), "type":"", "details":"", "pid":0, "pname":"", "img":[""]};
  infos: Info[];
  sharedInfo: Info;

  constructor(private storage: Storage, 
              private alertCtrl: AlertController,
              private toastCtrl: ToastController,  
            ) {
    console.log('Hello InfoDataProvider Provider');
  }

  infoAlert(msg: string){
    let alert = this.alertCtrl.create({
      title:"Info Saved",
      subTitle:msg,
      buttons:['Dismiss']
    });
    alert.present();
  }

  showToast(msg: string){
    let toastShow = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "middle"
    })
    toastShow.present();
  }
  saveInfo(info: Info): Promise<Info>{

    return new Promise(resolve=>{
      
    this.storage.get("infos").then((val)=>{
      console.log("I amhere. " + JSON.stringify(val));
      let status: boolean = false; 
      //console.log("Incoming Info: "+ JSON.stringify(val));
      if(val != null && val != undefined && val != "" && val != []){
        for(let i= 0; i < val.length; i++){
          if(val[i].id == info.id){
            val[i] = info;
            this.storage.set("infos",val);
            status = true;

            //this.infoAlert("Info Updated: " + JSON.stringify(info));
            this.showToast("Info Updated.")
            resolve(info);
          }
        }

        if(!status){
          let liid: number = val[val.length -1].id;
          info.id = liid + 1;
          val[val.length] = info;
          this.storage.set("infos",val);
          //this.infoAlert("New Info Added: " + JSON.stringify(info));
          this.showToast("New Info Added.")
          resolve(info);
        }
        
      }
      else{
        info.id = 1;
        let infos: Info[] = [info];
        this.storage.set("infos",infos);
        //this.infoAlert("First Info Created: " + JSON.stringify(info));

        this.showToast("First Info Created.")
        resolve(info);
      }     
    })
  })
}

  getInfoList(petid: number): Promise<Info[]> {
    return new Promise(resolve=>{
      this.storage.get("infos").then((val)=>{
      let finfos: Info[] = val;
      let rinfos: Info[];
      let j: number = 0;
      rinfos = finfos.filter(function(info){
        return info.pid == petid;
      })

       let descRinfos = rinfos.sort(function(a,b){
          return moment(b.date).valueOf() - moment(a.date).valueOf();
        })
     
      console.log("Returned List: " + JSON.stringify(descRinfos));
      resolve(descRinfos);
    })
    }
  )
}

  deleteAllInfo(){
    this.storage.remove("infos");
  }

  deleteInfo(info: Info):Promise<Info>{

    return new Promise(resolve=>{
    this.storage.get("infos").then((val)=>{
      let fInfo: Info[] = val;
      let nInfo: Info[];
      nInfo = fInfo.filter(function(dt){
        return dt.id != info.id;
      });
      this.storage.set("infos",nInfo).then(val=>{
        resolve(info);
        console.log("Info Delete Completed");
      });
    })
    }) 
  }

  deletePetInfo(petId: number, petName: string):Promise<boolean>{
    return new Promise(resolve=>{
      this.storage.get("infos").then((retInfos)=>{
        let nInfos = retInfos.filter(function(ret){
          return ret.pid != petId;           
        })

        this.storage.set("infos", nInfos).then(val=>{
          console.log("Set Infos Del Route Val: " + val);
          resolve(true);
        });
      })
    })

  }

  getAllInfo():Promise<Info[]>{
    return new Promise(resolve=>{
      this.storage.get("infos").then((val)=>{
      console.log("GetAllInf: " +JSON.stringify(val));
      resolve(val);
    })
    })
  }

  getPetInfo(pid: number): Promise<Info[]>{
    return new Promise(resolve=>{
      this.storage.get("infos").then((val)=>{
        
        let rinfos: Info[] = val.filter(function(va){
          return va.pid == pid;
        });       
        resolve(rinfos);
      })
    })

  }
  getInfo(id: number): Promise<Info>{
    return new Promise(resolve=>{
      this.storage.get("infos").then((val)=>{
      let rinfo: Info[] = val.filter(function(va){
        return va.id == id;
      });
      resolve(rinfo[0]);
    })
    })

  }

  setShareInfo(info: Info){
    this.sharedInfo = info;
  }

  getShareInfo(){
    let sInfo: Info = this.sharedInfo;
    this.sharedInfo = null;
    return sInfo; 
  }


}
