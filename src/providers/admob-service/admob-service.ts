import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig} from '@ionic-native/admob-free';
/*
  Generated class for the AdmobServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AdmobServiceProvider {

  constructor(public adMobFree: AdMobFree) {
    console.log('Hello AdmobServiceProvider Provider');
  }

  showBanner(){

    
    let bannerConfig: AdMobFreeBannerConfig =   {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-7415907348091573/3935872987"

      //ca-app-pub-7415907348091573/3935872987

    };

    this.adMobFree.banner.config(bannerConfig);
    this.adMobFree.banner.prepare().then(()=>{
      //this.adMobFree.banner.show();
      console.log("MyAdd");

    }).catch(e=>{
      console.log("MyError");
      console.log(e)
    });

    
  }

}
