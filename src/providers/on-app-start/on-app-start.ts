import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { ReminderServiceProvider } from '../reminder-service/reminder-service';
import { Storage } from '@ionic/storage';

import { ScheduleNotificationsProvider } from '../schedule-notifications/schedule-notifications';
 
/*
  Generated class for the OnAppStartProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class OnAppStartProvider {

  constructor(public reminderService: ReminderServiceProvider,
              private scheduleNotifications: ScheduleNotificationsProvider,
              private storage: Storage,
  ) {
    console.log('Hello OnAppStartProvider Provider');
  }


  appInitialRun(){
    this.storage.get("applaunchcount").then((val)=>{
      if(val){
        console.log("Not a First Run");
        let nVal: number =  val + 1;
        this.storage.set("applaunchcount",nVal);
        
        this.callScheduleNotifications();
        
      }
      else{
       // this.storage.set("pets",[]);
        //this.storage.set("infos",[]);
       /// this.storage.set("reminders",[]);
        this.storage.set("applaunchcount",1);
        console.log("App First Run Complete");
      }
    })
  }

  callScheduleNotifications(){
    this.scheduleNotifications.setNotifications(); 
    this.reminderService.getActiveReminders().then(va=>{
      if(va.length > 0){
         console.log("Active Reminders exist.");    
      }      
    })
    
    
  }

  

}
