import { Injectable, Input } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { ToastController } from 'ionic-angular';

import { Reminder } from '../../models/reminder';
import * as moment from 'moment';


/*
  Generated class for the ReminderServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ReminderServiceProvider {
  @Input()
  reminder: Reminder;

  constructor(private storage: Storage, private toastCtrl: ToastController) {
    console.log('Hello ReminderServiceProvider Provider');
  }

  showToast(msg: string){
    let toast = this.toastCtrl.create({
      message:msg,
      duration:3000

    });
    toast.present();
  }

  saveReminder(reminder: Reminder):Promise<Reminder>{
    return new Promise(resolve=>{
    this.storage.get("reminders").then((val)=>{
      let status: boolean = false;

      if(val != null && val != undefined && val != "" && val!= []){
        for(let i = 0; i < val.length; i++){
          if(val[i].id==reminder.id){
            val[i] = reminder;
            this.storage.set("reminders",val);
            status = true;
            console.log("Reminder Updated Successfully" + JSON.stringify(reminder));
            this.showToast("Reminder Updated Successfully" );
            //return reminder;
          }
        }

        if(!status){
          let lrid: number = val[val.length -1].id;
          reminder.id = lrid+1;
          val[val.length] = reminder;
          this.storage.set("reminders",val);
          console.log("New Reminder Created: " + JSON.stringify(reminder));
          this.showToast("New Reminder Created");
          //return reminder;

        }

      }else{
        reminder.id = 1;
        this.storage.set("reminders",[reminder]);
        console.log("First Reminder created Successfully" + JSON.stringify(reminder));
        this.showToast("First Reminder created Successfully");
        //return reminder;

      }
      resolve(reminder);    
    })
    })
  }

  getReminder(id: number): Promise<Reminder>{
    return new Promise(resolve=>{
      let rRem: Reminder[]; 
      this.storage.get("reminders").then((val)=>{
      rRem = val.filter(function(dt){
        return dt.id == id;
      });
      console.log("Get Reminder: " + JSON.stringify(rRem));
      resolve(rRem[0]); 
    })
    })
    
  };
    
  getReminderList(): Promise<Reminder[]>{
    return new Promise(resolve=>{
      this.storage.get("reminders").then((val)=>{
        console.log("Reminder List: " + JSON.stringify(val));
        let descRem; 
        if(val != null && val != undefined && val != []){        
        descRem = val.sort(function(a,b){
          return moment(b.date).valueOf() - moment(a.date).valueOf();
        })
        }
        resolve(descRem);
      })
      
    })
  }

  deleteAllReminders(){
    this.storage.remove("reminders");
    console.log("Deleted All Reminders");
  };

  deleteReminder(id: number):Promise<boolean>{
    return new Promise(resolve=>{
    this.storage.get("reminders").then((val)=>{
      let frem = val.filter(function(va){
        return va.id != id;
      });

    this.storage.set("reminders",frem).then(()=>{
      this.showToast("Reminder Deleted Successfully!");
      resolve(true)
    })  
    ;
  });


})};

  cleanReminders(){
    this.storage.get("reminders").then(val=>{
      
    })

    
  }

  getActiveReminders(): Promise<Reminder[]>{
    return new Promise(resolve=>{
      this.storage.get("reminders").then(remVal=>{
        let currRem = remVal.filter(rem=>{
          return (moment(rem.date).diff(moment(new Date()),"seconds") > 0); 
        })

        resolve(currRem);
      })
    })
    
  }

}
