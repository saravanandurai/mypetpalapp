import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DateProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DateProvider {

  constructor() {
    console.log('Hello DateProvider Provider');
  }

  getAge(d1: Date){
    
    let diff: number = (Date.now() -(d1.getTime()));
    let ddays: number = diff/(1000*60*60*24);
    let dyears: number = Math.floor(ddays/365);
    let remMonths: number = Math.floor((ddays%365)/30);    
    let ret: string;

    if(dyears<1){
      ret = (remMonths + " Months Old")
    }
    else{
      if(remMonths==0){
        if(dyears ==1){
          ret = (dyears + " Year Old");
        }else{
          ret = (dyears + " Years Old");
        }        
      }
      else{
        if(dyears ==1){
          if(remMonths ==1){
            ret = (dyears + " Year " + remMonths + " Month Old");
          }
          else{
            ret = (dyears + " Year " + remMonths + " Months Old");
          }
          
        }else{
          if(remMonths ==1){
            ret = (dyears + " Years " + remMonths + " Month Old");
          }
          else{
            ret = (dyears + " Years " + remMonths + " Months Old");
          }          
        }
      }
    } 
    console.log("Age: " + ret);
    return ret; 
  }

  getDateDiff(hd: Date, ld: Date){
    let ddiff = Math.floor((hd.getTime() - ld.getTime())/(1000*60*60*24));
    return ddiff;

  }

}
