import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Reminder } from '../../models/reminder';
import { ReminderServiceProvider } from '../../providers/reminder-service/reminder-service';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
import * as moment from 'moment';



 

/**
 * Generated class for the ReminderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reminder',
  templateUrl: 'reminder.html',
})
export class ReminderPage {
 @Input()
 reminder: Reminder = {"id":0, "date": moment(new Date()).format(),"pid":0, "pname": "", "type":"Vaccination", "details": "",
    "notifications": {
        "onTheDay": false,
        "oneDayBefore": false,
        "twoDaysBefore": false,
        "threeDaysBefore": false,
        "oneWeekBefore": false,
        "twoWeeksBefore": false,
        "threeWeeksBefore": false,
        "oneMonthBefore": false
    }
  };

  remFlag: boolean = true; 
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private reminderService: ReminderServiceProvider,
              private viewCtrl: ViewController,
              private admobServ: AdmobServiceProvider
             ) {
  }

  ionViewWillEnter(){
    this.admobServ.showBanner();

    this.reminderService.getReminder(this.navParams.get("id")).then((val)=>{
      console.log("Ret Val: " + JSON.stringify(val));
      this.reminder = val;
      if(moment(this.reminder.date).diff(moment(new Date(), "days")) <= 0){
        this.remFlag = false;
      }
    });


    
    if(this.navCtrl.getPrevious().name == "AddEditReminderPage" && this.navCtrl.getByIndex(0).name =="AddEditReminderPage" ){
      
      this.navCtrl.setPages([{page:"HomePage"},{page:"ReminderPage",params: {"id":this.navParams.get("id")}}]);
     
    }
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReminderPage');
  }

  editReminder(reminder: Reminder){
    this.navCtrl.push("AddEditReminderPage",{"id":reminder.id, "title":"Edit Reminder"});
  }

  deleteReminder(reminder: Reminder){
    this.reminderService.deleteReminder(reminder.id).then(val=>{
      console.log("Delete Reminder: " + val);
      this.navCtrl.pop();
    })

  }

}
