import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemindersListPage } from './reminders-list';

@NgModule({
  declarations: [
    RemindersListPage,
  ],
  imports: [
    IonicPageModule.forChild(RemindersListPage),
  ],
  exports: [
    RemindersListPage
  ]
})
export class RemindersListPageModule {}
