import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ReminderServiceProvider } from '../../providers/reminder-service/reminder-service';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';

import { Reminder } from '../../models/reminder';

/**
 * Generated class for the RemindersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-reminders-list',
  templateUrl: 'reminders-list.html',
})
export class RemindersListPage {
  @Input()
  reminders: Reminder[];
  remDataFlag: boolean = false;

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams,
                private reminderService: ReminderServiceProvider,
                private admobServ: AdmobServiceProvider,
                ) {
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter RemindersPage');
    this.admobServ.showBanner();    
    this.reminderService.getReminderList().then((val)=>{
      
      if(val != null && val != undefined && val != []){
        this.reminders = val;
        console.log("Returned Reminders: " + JSON.stringify(this.reminders));
      }else
      {
        this.remDataFlag = true;
      }
      
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RemindersPage');
  }

  callReminderPage(rem: Reminder){
    console.log("Edit Reminder");
    this.navCtrl.push("ReminderPage",{"id": rem.id});
  }

  deleteAllReminders(){
    this.reminderService.deleteAllReminders();
  }
}
