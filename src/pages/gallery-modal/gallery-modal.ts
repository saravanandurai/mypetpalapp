import { Component,Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Slides, AlertController } from 'ionic-angular';

/**
 * Generated class for the GalleryModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-gallery-modal',
  templateUrl: 'gallery-modal.html',
})
export class GalleryModalPage {
  //@Input()
  imgArr: any;
  currImg: string;
  currIndex: number;
  slidesFlag: boolean;
  edit: boolean;

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private alertCtrl: AlertController) {   

  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad GalleryModalPage');
    console.log("Curr Img: " + this.navParams.get("currImg"));
    console.log("Img Arr: " + JSON.stringify(this.navParams.get("imgArr")));
    this.currImg = this.navParams.get("currImg");

    this.imgArr = this.navParams.get("imgArr");

    for(let i=0; i < this.imgArr.length; i++){
      if(this.imgArr[i] == this.currImg){
        this.currIndex =i;
        break;
      }
    }
    this.slidesFlag = true;
    this.edit = this.navParams.get("edit");
    console.log("After Assign: " + JSON.stringify(this.imgArr));

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad GalleryModalPage');
  }

  dismiss(){
    let data  = {"imgArr": this.imgArr, "editFlag": this.edit};
    console.log("Dismiss Data: " + JSON.stringify(data));
    this.viewCtrl.dismiss(data);
  }

  delete(){
    let delAlert = this.alertCtrl.create({
      title:"Delete Photo",
      message:"Do you want to delete this photo?",
      buttons:[{
        text:'No',
        role:'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text:'Yes',
        handler: () => {
          console.log('Yes clicked');
          let currActInd = this.slides.getActiveIndex();
          let currActImg = this.imgArr[currActInd];
          let newImgArr = this.imgArr.filter(function(im){
            return im!=currActImg;
          });

          let nextPos: number;
          if(newImgArr.length==0){
            this.slidesFlag = false;
          }else{
            if(this.slides.isEnd()){
              nextPos = 0;
            }else{
              nextPos = currActInd;
            }
            
            this.imgArr = newImgArr;
            this.slides.update();
            this.slides.slideTo(nextPos);
            
            console.log("New ImgArr: " + JSON.stringify(this.imgArr));
          }
        }
      }
      ]
    });
    delAlert.present();
  }
  

}
