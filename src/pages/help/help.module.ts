import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpPage } from './help';

import {ExpandableComponentModule } from '../../components/expandable/expandable.module';

@NgModule({
  declarations: [
    HelpPage,
  ],
  imports: [
    ExpandableComponentModule,
    IonicPageModule.forChild(HelpPage),
  ],
  exports: [
    HelpPage
  ]
})
export class HelpPageModule {}
