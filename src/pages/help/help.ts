import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
/**
 * Generated class for the HelpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  
  expanded: Array<boolean> = [false, false,]; 
  

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams,
                public admobServ: AdmobServiceProvider   ) {
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter HelpPage');
    this.admobServ.showBanner();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

  expandItem(pos: number){
    
    this.expanded[pos] = !this.expanded[pos];
    
  }

}
