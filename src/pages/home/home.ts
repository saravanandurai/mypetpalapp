import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Pet } from '../../models/pet';

import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
import  { PetDataServiceProvider } from '../../providers/pet-data-service/pet-data-service';

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @Input()
  pets: Pet[];
  petFlag: boolean = true;
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private loadingCtrl: LoadingController,
              private admobServ: AdmobServiceProvider,
              private petData: PetDataServiceProvider,
            ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }


  ionViewWillEnter(){
    console.log("ionViewWillEnter HomePage");

    let loadSpinner = this.loadingCtrl.create({
      spinner:'bubbles',
      content:"Loading..."
    });
    loadSpinner.present();
    this.admobServ.showBanner();
    this.petData.getPetList().then((val)=>{
      
      
      console.log("Pet ListHome: " + JSON.stringify(this.pets));
      if(val == undefined || val == null){
        
        this.petFlag = false;
      }else{
        this.pets = val;
        this.petFlag = true;
      }
      if(val.length > 0 && val != undefined && val != null) 
        {          
          
        }else{
          
        }
      loadSpinner.dismiss();  
    });

    loadSpinner.dismiss();

  }

  addPet(){
    this.navCtrl.push('AddEditPetPage', {title:"Add Pet"});
  }

  callPet(pet: Pet){
    console.log("CallPet: " + JSON.stringify(pet));
    this.navCtrl.push('PetPage', pet);
  }
}
