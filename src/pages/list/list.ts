import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { ScheduleNotificationsProvider } from '../../providers/schedule-notifications/schedule-notifications';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  notes: Array<{id: number, text: string, at: string}> = [];
  nots: Array<{id: number, text: string, at: string}> = [];
  
  

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams,
                private scheduleNotifications: ScheduleNotificationsProvider,
                private admobServ: AdmobServiceProvider  
              ) {
    
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
   
    this.navCtrl.push(ListPage, {
     item: item
    });
  }


  //==============================================================

  ionViewWillEnter(){
    console.log("ionViewWillEnter ListsPage");
    this.admobServ.showBanner();
    
    this.scheduleNotifications.getAllNotifications().then(val=>{
      let valJSON = JSON.parse(JSON.stringify(val));
      this.notes = val;
      console.log();
      for(let n of this.notes){
        let dtStr: string = moment((Number(n.at))*1000).format();
        this.nots.push({"id":n.id, "text":n.text, "at":dtStr});
      }

      /*
      for(let i = 0; i < this.notes.length; i++){
         this.nots[i].id = this.notes[i].id;
         this.nots[i].text = this.notes[i].text;
         this.nots[i].at = moment((Number(this.notes[i].at))*1000).format();
         console.log("Nots Each: " + JSON.stringify(this.nots[i])); 
      }
        */
      /*
      for(let i=0; i < valJSON.length; i++){
        this.notes[i].id = valJSON[i].id;
        this.notes[i].text = valJSON[i].text;
        this.notes[i].at = valJSON[i].at;
        this.notes[i].atMoment = moment(valJSON[i].at).format();
        console.log("Moment: " + moment(valJSON[i].at).format());
      }

      */
      console.log("Note: " + JSON.stringify(this.notes))
    })

    
  }
}
