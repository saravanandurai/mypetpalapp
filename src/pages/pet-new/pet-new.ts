import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Pet } from '../../models/pet';
import { PET } from '../../constants/pet-constant';
import { PET_TYPE_LIST_CONSTANT } from '../../constants/pet-type-list-constant';
import { GENDER_CONSTANT } from '../../constants/gender-constant';
import * as moment from 'moment';

/**
 * Generated class for the PetNewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pet-new',
  templateUrl: 'pet-new.html',
})
export class PetNewPage {

  title:string = "";
  pet: Pet = PET;
  petTypes: Array<string> = PET_TYPE_LIST_CONSTANT;
  maxDt = moment(new Date()).format();
  genders: Array<string> = GENDER_CONSTANT;
  camStatus: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter PetNewPage");
    console.log("PET_TYPE : ", this.pet.type)
    if(this.navParams.get('title') == "Add Pet"){
      this.title = "Add Pet";
      this.camStatus = false;
    }else if(this.navParams.get('title') == "Edit Pet"){
      this.title = "Edit Pet";
    }else{
      this.title = "Pet";      
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PetNewPage');
  }

}
