import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditInfoPage } from './add-edit-info';
import { GalleryViewComponentModule } from '../../components/gallery-view/gallery-view.module';

@NgModule({
  declarations: [
    AddEditInfoPage,
  ],
  imports: [
    GalleryViewComponentModule,
    IonicPageModule.forChild(AddEditInfoPage),
  ],
  exports: [
    AddEditInfoPage
  ]
})
export class AddEditInfoPageModule {}
