import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';

import * as moment from 'moment';
import { Info } from '../../models/info';
import { InfoDataProvider } from '../../providers/info-data/info-data';
import { MyCameraProvider } from '../../providers/my-camera/my-camera';
import { PetDataServiceProvider } from '../../providers/pet-data-service/pet-data-service';
import { AlertPopupProvider } from '../../providers/alert-popup/alert-popup';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';


/**
 * Generated class for the AddEditInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-edit-info',
  templateUrl: 'add-edit-info.html',
})
export class AddEditInfoPage {
  @Input()
  info: Info = {"id":0, "date": moment(new Date()).format(), "type":"Vaccination", 
                "details":"", "pid":0, "pname":"", "img":[""]};
  title: string = "";
  maxDt: string = moment(new Date()).format();
  camStatus: boolean;
  galViewOptions: any = {"imgArr":this.info.img, "editFlag":true};
  petListFlag: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private infoData: InfoDataProvider,
              private alertCtrl: AlertController,
              private viewCtrl: ViewController,
              private myCamProvider: MyCameraProvider,
              private petDataService: PetDataServiceProvider,
              private alertPopup: AlertPopupProvider,
              private admobServ: AdmobServiceProvider,
              private loadingCtrl: LoadingController
              ) {
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter AddEditInfoPage');
    let loadingSpinner = this.loadingCtrl.create({
      spinner:'bubbles',
      content:"Loading..."

    });
    loadingSpinner.present();
    this.admobServ.showBanner();
    if(this.navParams.get("title") == "Add Info"){
      this.title = "Add Info";
      this.petDataService.getPetList().then((petList)=>{
        console.log("PetList: " + JSON.stringify(petList));
        if(petList != null && petList != undefined && petList != []){
          if(this.navParams.get("pet").id == null || this.navParams.get("pet").id == undefined){        
            console.log("I am here in PetSelAlrt Call");
            loadingSpinner.dismiss();
            this.alertPopup.petSelectAlert().then(sPet=>{
              let sPetObj = JSON.parse(sPet);
              this.info.pid = sPetObj.pid;
              this.info.pname = sPetObj.pname;
              console.log("Info: " + JSON.stringify(this.info));
              
            });      
          }else{
          this.info.pid = this.navParams.get("pet").id;
          this.info.pname = this.navParams.get("pet").name;
          loadingSpinner.dismiss();
          }

        }else{
          console.log("I am ggeere1");
          this.petListFlag = true;
          loadingSpinner.dismiss();

        }
      
      })
      
      this.camStatus = false;
      console.log("I am in Add Info Routing");

    }
    else{
      this.title = "Edit Info";
      this.infoData.getInfo(this.navParams.get("id")).then((val)=>{
        this.info = val;
        
        this.galViewOptions = {"imgArr":this.info.img, "editFlag":true};
        if(this.info.img.length > 0){
        this.camStatus = true;
        }else{
        this.camStatus = false;  
        }
        console.log("I am in Edit Info Routing");
        loadingSpinner.dismiss();
        
      })
    }
    /*
    
    if (this.navCtrl.getPrevious().name == "InfoPage"){
      this.title = "Edit Info";
      this.infoData.getInfo(this.navParams.get("id")).then((val)=>{
        this.info = val;
        
        this.galViewOptions = {"imgArr":this.info.img, "editFlag":true};
        if(this.info.img.length > 0){
        this.camStatus = true;
        }else{
        this.camStatus = false;  
        }
        console.log("I am in Edit Info Routing");
        loadingSpinner.dismiss();
        
      })
    }else{     
      this.title = "Add Info";
      this.petDataService.getPetList().then((petList)=>{
        console.log("PetList: " + JSON.stringify(petList));
        if(petList != null && petList != undefined && petList != []){
          if(this.navParams.get("id") == null || this.navParams.get("id") == undefined){        
            console.log("I am here in PetSelAlrt Call");
            loadingSpinner.dismiss();
            this.alertPopup.petSelectAlert().then(sPet=>{
              let sPetObj = JSON.parse(sPet);
              this.info.pid = sPetObj.pid;
              this.info.pname = sPetObj.pname;
              console.log("Info: " + JSON.stringify(this.info));
              
            });      
          }else{
          this.info.pid = this.navParams.get("id");
          this.info.pname = this.navParams.get("name");
          loadingSpinner.dismiss();
          }

        }else{
          console.log("I am ggeere1");
          this.petListFlag = true;
          loadingSpinner.dismiss();

        }
      
      })
      
      this.camStatus = false;
      console.log("I am in Add Info Routing");
    }
    */
    
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEditInfoPage');
  }

  saveInfo(){
    console.log("Save Info: " + JSON.stringify(this.info));
    
    this.infoData.saveInfo(this.info).then(val=>{
    
    if(this.title == "Add Info"){
      console.log("Add Route");     
      if(this.navCtrl.getViews().length == 1){
        this.navCtrl.setPages([{page:"MoreInfoPage", params:{"pid":val.pid, "pname":val.pname}},{page:"InfoPage",params:{"id":val.id}}]);
      }else{
           
        this.viewCtrl.dismiss(); 
        this.navCtrl.push("InfoPage", val);
      }   
        
    }else{
      console.log("Edit Route");
      
      this.navCtrl.pop();  
    }
    });
    
  }

  deleteInfo(info: Info){
    console.log("Clear Info");
    if(info.id==0){
      this.showAlert("Do you want to clear the entry?",false);
    }else{

      this.showAlert("Do you want to delete this info?",true);
    }

  }

  addPhoto(){
    console.log("Add Photo Fn");
    let imgCount: number;
    if(this.info.img[0] != ""){
      imgCount = this.info.img.length
    }else{
      imgCount = 0;
    }
    
    
    this.myCamProvider.mulCamorGalSelector(imgCount).then((val)=>{
      console.log("Returned Photo Val: " + JSON.stringify(val));
      if(val.error == false && val.base64Image.length > 0){
        if(this.info.img[0] == "")
        {
          this.info.img = val.base64Image;
        }else{
          for(let eachImg of val.base64Image)
          this.info.img.push(eachImg);
        }
                
        
        this.galViewOptions = {"imgArr":this.info.img, "editFlag":true}; 
        this.camStatus = val.camStatus;      
      }
    })
    
  }

  showAlert(msg: string, status: boolean){
    let dalert = this.alertCtrl.create({
      title:"Delete Info",
      message:msg,
      buttons:[
        {
        text:"Yes",
        role:null,
        handler:()=>{
          if(status == false){
            let pid: number = this.info.pid;
            let pname: string = this.info.pname;
            this.info = {"id":0, "date": moment(new Date()).format(), "type":"Vaccination", "details":"", "pid":pid, "pname":pname, "img":[""]};
            this.camStatus=false;
          }
          else{
            let pid: number = this.info.pid;
            let pname: string = this.info.pname;
            this.title = "Add Info";
            this.infoData.deleteInfo(this.info).then(val=>{
            this.info = {"id":0, "date": moment(new Date()).format(), "type":"Vaccination", "details":"", "pid":pid, "pname":pname, "img":[""]};
              
            this.navCtrl.popTo(this.navCtrl.getByIndex(this.viewCtrl.index - 2));            
            this.camStatus=false;
            });
          }
        }},{
          text: "No",
          role:"cancel",
          handler:()=>{

          }
        }]
    });

    dalert.present();
  }

  refreshData(ev){
    console.log("Emitted Event: " + ev);
    this.galViewOptions = ev;
    this.info.img = this.galViewOptions.imgArr;
  }

   
  

  petNullValidator():boolean{
    if(this.info.pid != null && this.info.pid != undefined && this.info.pid != 0){
      return true; 
    } else {
      return false;
    }
  }

}
