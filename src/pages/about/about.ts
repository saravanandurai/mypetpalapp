import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams,
                public admobServ: AdmobServiceProvider,
              ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter AboutPage');
    this.admobServ.showBanner();
  }

}
