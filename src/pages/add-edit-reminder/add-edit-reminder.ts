import { Component, Input, OnChanges  } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Reminder } from '../../models/reminder';
import { ReminderServiceProvider } from '../../providers/reminder-service/reminder-service';
import { PetDataServiceProvider } from '../../providers/pet-data-service/pet-data-service';
import { ScheduleNotificationsProvider } from '../../providers/schedule-notifications/schedule-notifications';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';

import * as moment from 'moment';
import { PetSelect } from '../../models/petSelect';

/**
 * Generated class for the AddEditReminderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-edit-reminder',
  templateUrl: 'add-edit-reminder.html',
})
export class AddEditReminderPage{
  
  @Input()
  reminder: Reminder = {"id":0,"date":this.minDate(),"pid":0, "pname":"", "type":"Vaccination", "details":"",
                          "notifications":{
                            "onTheDay":true,
                            "oneDayBefore": false,
                            "twoDaysBefore": false,
                            "threeDaysBefore": false,
                            "oneWeekBefore": false,
                            "twoWeeksBefore": false,
                            "threeWeeksBefore": false,
                            "oneMonthBefore": false
                          }
                          };
  remConst:  Reminder = {"id":0,"date":this.minDate(),"pid":0, "pname":"", "type":"Vaccination", "details":"",
                          "notifications":{
                            "onTheDay":true,
                            "oneDayBefore": false,
                            "twoDaysBefore": false,
                            "threeDaysBefore": false,
                            "oneWeekBefore": false,
                            "twoWeeksBefore": false,
                            "threeWeeksBefore": false,
                            "oneMonthBefore": false
                          }
                          };

  notificationFlag: Array<boolean> =[false, false, false,false,false,false,false,false]; 
 
                
  notTime:number = 12;
  petDtl: PetSelect[] = [{"id":0,"name":"Others"}]
  title: string = "Add Reminder";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public reminderService: ReminderServiceProvider,
              public petDataService: PetDataServiceProvider,
              public alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public scheduleNotifications: ScheduleNotificationsProvider,
              private admobServ: AdmobServiceProvider,
            ){

  }    
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEditReminderPage');
  }

  ionViewWillEnter(){
    console.log("IonViewWillEnter AddEditReminderPage");  
    this.admobServ.showBanner();
    if(this.navParams.get("title") == "Add Reminder"){
      this.reminder.date = this.minDate();
      this.dateDisplayCtrl(this.reminder.date);
      if(this.navParams.get("pid")){
        this.reminder.pid = this.navParams.get("pid");
      }else{
        this.reminder = this.remConst;
      }       
    }
    else{     
      this.title = "Edit Reminder";      
      this.reminderService.getReminder(this.navParams.get("id")).then(val=>{
      this.reminder = val;
      this.dateDisplayCtrl(this.reminder.date);
      });
    }
     
    this.petDataService.getPetList().then(val=>{
      
      if(val != null && val != undefined && val!= []){
        for(let i = 0; i < val.length; i++){
          this.petDtl[i+1] = {"id":val[i].id, "name": val[i].name};
        }        
      }
      
    });    
  }

  minDate():string{
    let minDt = moment(new Date());    
    if(minDt.hour() < this.notTime){
      return minDt.format();
    }else{
      return minDt.add(1, 'day').format();
    }
  }

  cancelReminder(){
    console.log("Cancel Reminder");
   
    if(this.title == "Add Reminder"){
      console.log("Cancel Reminder Add Reminder");
      this.alertPop("Do you want to clear the entry?");
    }else{
      console.log("Cancel Reminder Edit Reminder");
      this.alertPop("Do you want to delete this reminder?");
    }

  }

  alertPop(msg: string){
    let cancelAlert = this.alertCtrl.create({
      title: "Cancel Reminder",
      message: msg,
      buttons:[
        {
          text: "No",
          role: "cancel",
          handler:()=>{
            console.log("Cancel Clicked");
          }
        },
        {
            text: "Yes",
            handler:()=>{
              if(this.title == "Add Reminder"){
                console.log("AddRemAlert");
                this.reminder = JSON.parse(JSON.stringify(this.remConst));                                               
              }else{
                this.reminderService.deleteReminder(this.reminder.id).then((delRemVal)=>{
                  if(delRemVal == true){
                  console.log("Reminder Deleted Successfully");
                  this.scheduleNotifications.setNotifications(); 
                  this.reminder = JSON.parse(JSON.stringify(this.remConst));
                  console.log("I am here to move");
                  this.navCtrl.pop();
                  this.navCtrl.pop();
                                
                  }else{
                    console.log("Snap! Something broke in reminder Service delete");
                  }
                });
                
              }
            }
        }

      ]
    });
    cancelAlert.present();

  }

  saveReminder(){
    console.log("Save Reminder: " + JSON.stringify(this.reminder));
    let selPid = this.reminder.pid;
    let selPet = this.petDtl.filter(function(ptDt){
                    return ptDt.id == selPid;
                  });
    this.reminder.pname = selPet[0].name;
    let sDt = moment(this.reminder.date);
    sDt.set({"hour":12, "minute":0, "second":0, "millisecond":0 }); 
    this.reminder.date = sDt.format();
    
    this.reminderService.saveReminder(this.reminder).then(val=>{
      console.log("Reminder Saved: " + JSON.stringify(val));
      this.scheduleNotifications.setNotifications();

      if(this.title== "Add Reminder"){
        if(this.navCtrl.getViews().length == 1){
          this.navCtrl.setPages([{page:"RemindersListPage"},{page:"ReminderPage", params:{"id":val.id}}]);
        }else{
          
          this.viewCtrl.dismiss();
          this.navCtrl.push("ReminderPage", {"id":val.id});
          
        }
      }else{
        this.navCtrl.pop();
      }    
    })

  }

  dateDisplayCtrl(dt: string){
      console.log("SetDt: " + dt);
      let setDt = moment(dt);
      setDt.set({"hour":this.notTime, "minute":0, "second":0, "millisecond":0 });
      let currDt = moment(new Date());      
      let diff: number = setDt.diff(currDt, 'days');
      let diffM: number = setDt.diff(currDt, 'months');
      console.log("diffM: " + diffM );
      let setNots: any = JSON.parse(JSON.stringify(this.reminder.notifications));      
      this.reminder.notifications = JSON.parse(JSON.stringify(this.remConst.notifications));      
      this.notificationFlag = [false, false, false, false, false, false, false, false];      
      if(diff >= 0){
        this.notificationFlag[0] = true;
        if(setNots.onTheDay == true)this.reminder.notifications.onTheDay = true;        
      }      
      if(diff >= 1){
        this.notificationFlag[1] = true;
        if(setNots.oneDayBefore == true)this.reminder.notifications.oneDayBefore = true;
      }
      if(diff >= 2){
        this.notificationFlag[2] = true;
        if(setNots.twoDaysBefore == true)this.reminder.notifications.twoDaysBefore = true;
      }
      if(diff >= 3){
        this.notificationFlag[3] = true;
        if(setNots.threeDaysBefore == true)this.reminder.notifications.threeDaysBefore = true;
      }
      if(diff >= 6){
        this.notificationFlag[4] = true;
        if(setNots.oneWeekBefore == true)this.reminder.notifications.oneWeekBefore = true;
      }
      if(diff >= 13){
        this.notificationFlag[5] = true;
        if(setNots.twoWeeksBefore == true)this.reminder.notifications.twoWeeksBefore = true;
      }
      if(diff >= 20){
        this.notificationFlag[6] = true;
        if(setNots.threeWeeksBefore == true)this.reminder.notifications.threeWeeksBefore = true;
      }
      if(diffM >= 1){
        this.notificationFlag[7] = true;
        if(setNots.oneMonthBefore == true)this.reminder.notifications.oneMonthBefore = true;
      }
      
           
  }

  notificationStatus():boolean{
    if( this.reminder.notifications.onTheDay == true || this.reminder.notifications.oneDayBefore == true || 
        this.reminder.notifications.twoDaysBefore == true || this.reminder.notifications.threeDaysBefore == true || 
        this.reminder.notifications.oneWeekBefore == true || this.reminder.notifications.twoWeeksBefore == true || 
        this.reminder.notifications.threeWeeksBefore == true || this.reminder.notifications.oneMonthBefore == true
      ){
        return true;
      }else{
        return false;
      }
  }


  

  

}
