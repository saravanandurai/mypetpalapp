import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
//import { FormsModule } from '@angular/forms';
import { AddEditReminderPage } from './add-edit-reminder';

@NgModule({
  declarations: [
    AddEditReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditReminderPage),
    //FormsModule
  ],
  exports: [
    AddEditReminderPage
  ]
})
export class AddEditReminderPageModule {}
