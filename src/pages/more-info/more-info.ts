import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';


import { InfoDataProvider} from '../../providers/info-data/info-data';
import { Info } from '../../models/info';
import { AlertPopupProvider } from '../../providers/alert-popup/alert-popup';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';

/**
 * Generated class for the MoreInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-more-info',
  templateUrl: 'more-info.html',
})
export class MoreInfoPage {
  @Input()
  infos: Info[];
  pname: string;
  infoDataFlag: boolean = false;
  petInfoDataFlag: boolean = false;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private infoData: InfoDataProvider,
              private alertPopup: AlertPopupProvider,  
              private admobServ: AdmobServiceProvider,  
              private loadingCtrl: LoadingController,
              ) {
  }

  ionViewWillEnter(){
    let loadingSpinner = this.loadingCtrl.create({
      spinner:'bubbles',
      content:"Loading..."

    });
    loadingSpinner.present();

    this.admobServ.showBanner();
    this.infoData.getAllInfo().then(infolst=>{

      if(infolst != null && infolst != undefined && infolst != []){
        if(this.navParams.get("pid") == null || this.navParams.get("pid") == undefined || this.navParams.get("pid") == 0)
        {
          console.log("1");
          loadingSpinner.dismiss();
          this.alertPopup.petSelectAlert().then(val=>{
              let sPetObj = JSON.parse(val);
              this.pname = sPetObj.pname;
              this.infoData.getPetInfo(sPetObj.pid).then(va=>{
                if(va.length >0){
                  console.log("va.data: " + JSON.stringify(va));
                  this.infos = va;
                }else{
                  this.petInfoDataFlag = true;
                }
                
              })
            });
          
          }else{
            console.log("2");
            this.pname = this.navParams.get("pname");
            this.infoData.getPetInfo(this.navParams.get("pid")).then((val)=>{
              if(val.length > 0){
                this.infos = val;
                loadingSpinner.dismiss();
              }else{
                this.petInfoDataFlag = true;
                loadingSpinner.dismiss();                
              }
            
          });
            
        }

      }
      else{
        this.infoDataFlag = true;
        loadingSpinner.dismiss();

      }
      
    })

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoreInfoPage');
  }

  detailedInfo(info: Info){
    console.log("Push to Info");
    this.navCtrl.push("InfoPage",{"id":info.id});
  }
}
