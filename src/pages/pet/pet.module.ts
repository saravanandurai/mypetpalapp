import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PetPage } from './pet';

@NgModule({
  declarations: [
    PetPage,
  ],
  imports: [
    IonicPageModule.forChild(PetPage),
  ],
  exports: [
    PetPage
  ]
})
export class PetPageModule {}
