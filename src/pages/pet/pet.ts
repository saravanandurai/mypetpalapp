import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { PetDataServiceProvider } from '../../providers/pet-data-service/pet-data-service';
import { DateProvider } from '../../providers/date/date';
import { InfoDataProvider } from '../../providers/info-data/info-data';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';

import { Pet } from '../../models/pet';
import { Info } from '../../models/info';


/**
 * Generated class for the PetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pet',
  templateUrl: 'pet.html',
})
export class PetPage {
 @Input() 
 pet: Pet;
 status: boolean = false;
 statusInfo: boolean = false;
 age: string;
 infos: Info[];
 
  constructor(
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private petData: PetDataServiceProvider,
              private dateProv: DateProvider,
              private infoData: InfoDataProvider,
              private admobServ: AdmobServiceProvider,
              private loadingCtrl: LoadingController,
            ) {
        
    
  }

  ionViewWillEnter(){
    console.log("IonViewWillEnter PetPage");
    let loadingSpinner = this.loadingCtrl.create({
       spinner:'bubbles',
      content:"Loading..."
    });
    loadingSpinner.present();
    this.admobServ.showBanner();
    
    this.petData.getPet(this.navParams.get("id"),this.navParams.get("name")).then((val)=>{
        console.log("Data: " + JSON.stringify(this.pet));
        this.pet = val;
        this.status = true;
        console.log("Data2: " + JSON.stringify(this.pet)); 
        let dt: Date = new Date(this.pet.dob);
        this.age = this.dateProv.getAge(dt);        
        this.infoData.getInfoList(val.id).then((val1)=>{
          this.infos = val1;
          console.log("InfoList1: " + JSON.stringify(this.infos.length));
          if(this.infos.length > 0){
          this.statusInfo = true;
          }
          
        });
        
        
          
      });          
      console.log("Previous View: " + this.navCtrl.getPrevious().name); 
      loadingSpinner.dismiss();
    }  
  



  ionViewDidLoad() {
    console.log('ionViewDidLoad PetPage');
  }

  editPet(pet: Pet){
    this.navCtrl.push("AddEditPetPage", pet);
  }

  addInfo(pet: Pet){
    this.navCtrl.push("AddEditInfoPage", {"title":"Add Info", "pet": pet});
  }
  
  callMoreInfo(pet: Pet){
    this.navCtrl.push("MoreInfoPage",{"pid":pet.id, "pname":pet.name});
  }
  //This may have to be deleted
  callInfo(info: Info){
    this.navCtrl.push("InfoPage", info);
  }

  

}
