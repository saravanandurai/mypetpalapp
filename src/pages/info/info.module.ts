import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoPage } from './info';

import { GalleryViewComponentModule } from '../../components/gallery-view/gallery-view.module';

@NgModule({
  declarations: [
    InfoPage,
  ],
  imports: [
    GalleryViewComponentModule,
    IonicPageModule.forChild(InfoPage),
  ],
  exports: [
    InfoPage
  ]
})
export class InfoPageModule {}
