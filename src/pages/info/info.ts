import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Info } from '../../models/info';
import { InfoDataProvider } from '../../providers/info-data/info-data';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';




/**
 * Generated class for the InfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  @Input()
  info: Info;
  status: boolean = false;
  galViewOptions: any = {"imgArr":[""], "editFlag":false};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private infoData: InfoDataProvider,
              private admobServ: AdmobServiceProvider,
              private loadingCtrl: LoadingController,
            ) {
    
  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter InfoPage");

    let loadingSpinner = this.loadingCtrl.create({
      spinner:'bubbles',
      content:"Loading..."

    });
    loadingSpinner.present();

    this.admobServ.showBanner();
    /*
    if(this.navCtrl.getPrevious().name == "AddEditInfoPage"){
      console.log("Remove AddEditInfoPage: " + this.navCtrl.getPrevious().index);
      this.navCtrl.removeView(this.navCtrl.getPrevious());
    }
    */
    
    this.infoData.getInfo(this.navParams.get("id")).then((val)=>{
      this.info = val;
      this.status = true;
      console.log("Previous: " + this.navCtrl.getPrevious().name);
      console.log("Info Returned: " + JSON.stringify(this.info));
      this.galViewOptions = {"imgArr":this.info.img, "editFlag":false};
      loadingSpinner.dismiss();
      
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

  editInfo(){
    console.log("Edit Info");
    this.navCtrl.push('AddEditInfoPage',this.info);
    
    
    
    
  }

  setReminder(){    
    console.log("Set Reminder");
    this.navCtrl.push('AddEditReminderPage',{"title":"Add Reminder", "pid":this.info.pid});    
  }

  refreshData(ev){
    console.log("Emitted Event: " + JSON.stringify(ev));
    this.galViewOptions = ev;
  }

}
