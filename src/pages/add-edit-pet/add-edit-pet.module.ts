import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditPetPage } from './add-edit-pet';

@NgModule({
  declarations: [
    AddEditPetPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditPetPage),
  ],
  exports: [
    AddEditPetPage
  ]
})
export class AddEditPetPageModule {}
