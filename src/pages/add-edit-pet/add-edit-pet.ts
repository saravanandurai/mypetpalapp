import { Component, Input} from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { PetDataServiceProvider } from '../../providers/pet-data-service/pet-data-service';
import { InfoDataProvider } from '../../providers/info-data/info-data';
import { MyCameraProvider } from '../../providers/my-camera/my-camera';
import { AdmobServiceProvider } from '../../providers/admob-service/admob-service';
import * as moment from 'moment';

import { Pet } from '../../models/pet';




/**
 * Generated class for the AddEditPetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add-edit-pet',
  templateUrl: 'add-edit-pet.html',
})
export class AddEditPetPage {

 @Input()

  pet: Pet = {"id":0,"name":"","dob":moment(new Date()).format(),"type":"Dog","breedName":"", "gender":"Male", "img":"" };
  
  title: string = "Add Pet";  
  base64Image: string;   
  camStatus: boolean;
  maxDt: string =  moment(new Date()).format();
  //camAuth: any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController, 
              public navParams: NavParams, 
              private petData: PetDataServiceProvider, 
              private formBuilder: FormBuilder,
              private alertCtrl: AlertController,
              private myCamProv: MyCameraProvider,
              private admobServ: AdmobServiceProvider,
              private infoData: InfoDataProvider,
              private loadingCtrl: LoadingController
              
              ) {          
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter AddEditPetPage');

    let loadingSpinner = this.loadingCtrl.create({
      spinner:'bubbles',
      content:"Loading..."
    })
    loadingSpinner.present();
    this.admobServ.showBanner();
    
   
    //if(this.navCtrl.getPrevious().name == "PetPage"){

      

      console.log("navParams Data: " + JSON.stringify(this.navParams.data));
     if(this.navParams.get("title") == "Add Pet"){
       console.log("I am here");
        this.title = "Add Pet";
        this.camStatus = false;   
        loadingSpinner.dismiss();
     }else{
        console.log("I am EDit Pet");
        this.petData.getPet(this.navParams.get("id"),this.navParams.get("name")).then((val)=>{
        this.pet=val;
        this.title = "Edit Pet";
        this.camStatus = true;
        loadingSpinner.dismiss();
      
        });
       

     } 

     /*
    if(this.navParams.get("id") != null && this.navParams.get("id") != undefined){
    this.petData.getPet(this.navParams.get("id"),this.navParams.get("name")).then((val)=>{
      this.pet=val;
      this.title = "Edit Pet";
      this.camStatus = true;
      loadingSpinner.dismiss();
      
    });
    
    
    }    
    else{
      
      this.title = "Add Pet";
      this.camStatus = false;   
      loadingSpinner.dismiss();   
    }
*/
    

  }

  
  

  saveAddPet(pet: Pet){       
    this.petData.savePet(pet).then((val)=>{
    console.log("DPet: " + JSON.stringify(val));
    if(this.title=="Add Pet"){
      console.log("I am her");
      if(this.navCtrl.getViews().length == 1){
        this.navCtrl.setPages([{page: "HomePage"},{page:"PetPage",params:val}])
      }else{
        this.navCtrl.pop();
        this.navCtrl.push("PetPage", val);
      }
      
    } else
    {
    this.navCtrl.pop();
    
    }
    })   
  }

  cancelAddPet(){
    //this.navCtrl.pop();
    console.log("Cancel AddEditPet");
    if(this.pet.id==0){
      this.showPetAlert("Do you want to clear entered pet information?",false);
    }else{
      let msg: string = "Do you want to permanently delete pet " + this.pet.name + "  and all associated Info data."; 
      this.showPetAlert(msg,true);
      
    }
    
    
    /*
    this.petData.getPetList().then((val)=>{
      console.log("Got Pet Val: " + JSON.stringify(val));    
    });

    */
  }

  
  showPetAlert(msg: string, flag: boolean){
    let petAlert = this.alertCtrl.create({

                    title:"Delete Pet",
                    message:msg,
                    buttons:[{
                      text:"Yes",
                      role: null,
                      handler:()=>{
                        if(flag == false){
                          console.log("Pet Del False");
                          this.pet = {"id":0,"name":"","dob":moment(new Date()).format(),"type":"","breedName":"", "gender":"", "img":"" };
                          this.camStatus = false;
                        }
                        else
                        {
                          this.petData.deletePet(this.pet).then((val)=>{
                            if(val==true){
                              console.log("Pet Del True");
                              this.infoData.deletePetInfo(this.pet.id, this.pet.name).then((retVal)=>{
                                this.pet = {"id":0,"name":"","dob":moment(new Date()).format(),"type":"","breedName":"", "gender":"", "img":"" };
                                this.camStatus=false;
                                this.navCtrl.popTo(this.navCtrl.getByIndex(this.viewCtrl.index - 2));
                              });
                              
                              //this.navCtrl.setPages([{page:HomePage},{page:AddEditPetPage,params:{"title":"Add Pet"}}]);
                              

                            }
                          });
                          
                        }
                      }
                    },
                    {
                      text:"No",
                      role:"cancel",
                      handler:()=>{}
                    }]

                  });
    petAlert.present();              
                  

  }

  addPhoto(){
    
    console.log("Add Photo");
    
    this.myCamProv.CamOrGalSelector().then((val)=>{      
      

      console.log("Second Base64: " + JSON.stringify(val));
            
      if(val.error == false){
        this.base64Image = val.base64Image;        
        console.log("this.base64Image != null && this.base64Image != undefined");
        this.camStatus  = true;
        this.pet.img = this.base64Image;
        
      

      }
      /*
      else{
        console.log("Error in Cam Provider: " + val.errorMsg);
      }
      */

      
    });
    
   
  }

                                
                             

}


