import { Pet } from '../models/pet'; 
import { PET_TYPE_LIST_CONSTANT } from '../constants/pet-type-list-constant';
import { GENDER_CONSTANT } from '../constants/gender-constant';
import * as moment from 'moment';
export const PET: Pet = {
    id: 0,
    name : "",
    dob: moment(new Date()).format(),
    type: PET_TYPE_LIST_CONSTANT[0],
    breedName: "",
    gender: GENDER_CONSTANT[0],
    img: ""
}